package com.mycode.zxing;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;

/**
 * 生成二维码
 * */
public class ZxingQRCodeUtil {
    /**
     * @author shaoyifei
     * @param content  二维码内容
     * @param format   格式
     * 使用zxing方法生成二维码
     * */
    public static void createCode(String content,String format){
        //设置图片尺寸
        int width = 300;
        int height = 300;

        //设置二维码参数
        HashMap hints = new HashMap();
        hints.put(EncodeHintType.CHARACTER_SET,"utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION,ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN,2);

        try {
            //将二维码参数写入MultiFormat中
            BitMatrix bitMatrix =  new MultiFormatWriter().encode(content,BarcodeFormat.QR_CODE,width,height);
            Path file = new File("D:/img.png").toPath();
            //注意：输出方式可以有很多种，例如输出流，可以直接输出到网页中
            MatrixToImageWriter.writeToPath(bitMatrix,format,file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @author shaoyifei
     * @param path  二维码文件路径
     * 使用zxing方法解析二维码
     * */
    public static void analyseCode(String path){
        //二维码图片读入流
        MultiFormatReader reader = new MultiFormatReader();
        //指定文件路径
        File file = new File(path);
        //将文件转为一个imag格式
        try {
            BufferedImage image = ImageIO.read(file);

            //将image转为二进制位图bitmap
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));

            //定义二维码参数
            HashMap hints = new HashMap();
            hints.put(EncodeHintType.CHARACTER_SET,"utf-8");

            //获取解析结果
            Result result = reader.decode(binaryBitmap,hints);
            System.out.println("解析结果："+result.toString());
            System.out.println("编码格式："+result.getBarcodeFormat());
            //System.out.println(result.getResultPoints());
            System.out.println("文本内容："+result.getText());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

    }
}
