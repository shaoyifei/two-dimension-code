package com.mycode.qrcode;
/**
 * 一个奇怪的现象，QRCode方式生成的二维码，无法被该方式的decode方法解析，解析出的结果是一串数字，
 * 但是该方法可以解析zxing方式生成的二维码，且解析结果正确，可能是二维码生成过程中编码问题
 * */
import com.swetake.util.Qrcode;
import jp.sourceforge.qrcode.QRCodeDecoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 使用java画板生成二维码
 * */
public class QRCodeUtil {
   //QRCode的核心类，用于定义二维码参数等信息
   Qrcode qr = new Qrcode();
   //声明一个缓存imag对象，用于绘画
   BufferedImage bufferedImage;
   int width = 67+12*(7-1);
   int height = 67+12*(7-1);;

   /**
    * 使用Qrcode生成二维码
    * @author
    * @param content  二维码内容
    * @param  imageFormat  图片格式
    * */
   public void createQRCode(String content, String imageFormat,String path){
      qr.setQrcodeErrorCorrect('M');
      qr.setQrcodeEncodeMode('B');//A表示a-Z，N代表数字，B代表其他字符（中文日文等）
      qr.setQrcodeVersion(7);
      //内容
      String qrData = content;

      //定义缓存画板的尺寸及格式
      bufferedImage = new BufferedImage(width,height,BufferedImage.TYPE_3BYTE_BGR);

      //开始画图
      Graphics2D gs = bufferedImage.createGraphics();
      //定义画板属性
      gs.setBackground(Color.white);
      gs.setColor(Color.BLACK);
      //清空画板内容
      gs.clearRect(0,0,width,height);

      //增加偏移量，避免解析出错
      int pixOff = 2;

      //将内容转为字节数组
      byte[] qrbytes = qrData.getBytes();    //可以设置中文编码
      if(qrbytes.length>0&&qrbytes.length<120){
         //依据字节数组计算得到一个boolean矩阵
         boolean[][] bls = qr.calQrcode(qrbytes);
         for(int i = 0; i < bls.length; i++){
            for(int j = 0; j < bls.length; j++){
               if(bls[i][j]){
                  //在矩阵中为true的位置画点
                  gs.fillRect(j*3+pixOff,i*3+pixOff,3,3);
               }
            }
         }
      }
      //关闭画板
      gs.dispose();
      //buffer刷出
      bufferedImage.flush();
      //将bufferimage内容写入文件
      try {
         ImageIO.write(bufferedImage,imageFormat,new File(path));
      } catch (IOException e) {
         e.printStackTrace();
      }

   }

   /**
    * 使用Qrcode解析二维码图片
    * @author
    * @param  path 二维码文件路径
    *
    * */
   public void analyseQr(String path){
      //将文件路径封装为File，使用ImageIO的read方法读取file，生成一个bufferImage
      try {
         bufferedImage = ImageIO.read(new File(path));
      } catch (IOException e) {
         e.printStackTrace();
      }

      //调用QRCode中的DeCode方法
      QRCodeDecoder qrCodeDecoder = new QRCodeDecoder();
      //由于工具中的QRCodeImage是一个接口，在MyQRCodeImage中实现并重写方法
      //使用decode方法解析二维码文件，返回时一个byte数组
      String reslt = null;
      try {
         reslt = new String(qrCodeDecoder.decode(new MyQRCodeImage(bufferedImage)),"UTF-8");
      } catch (UnsupportedEncodingException e) {
         e.printStackTrace();
      }
      System.out.println(reslt);

   }
}
